.PHONY: fix

fix:
	rm -rf vendor/
	rm composer.json composer.lock
	composer require --no-interaction friendsofphp/php-cs-fixer:~2.16.0
	vendor/bin/php-cs-fixer fix -v
	rm composer.json composer.lock
	git checkout -- composer.json
	git checkout -- composer.lock
	composer install -n
