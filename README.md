## Installation (for KISPHP platform)

Update composer.json:

```json
{
    ...
    "require": {
        ...
        "kisphp/media-files-bundle": "~0.1",
        ...
    },
    ...
    "repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:kisphp/media-files-bundle.git"
        }
    ]
}
```

Register bundle

```php
// app/AppKernel.php
public function registerBundles()
{
    $bundles = [
        ...
        new \MediaFilesBundle\MediaFilesBundle(),
    ];
}
```

Add config parameters
```yaml
# add in app/config/parameters.yml
media_files_path: /path/to/_media_files
media_files_url: 'http://media.server'
allow_media_files_override: true # or false
```

Add routing in `app/config/routing.yml`
```yaml
media_files:
    resource: "@MediaFilesBundle/Resources/config/routing.yml"
    prefix:   /media

adm_media_thumbs:
    path: /media-thumbs/{path}.{_format}
    defaults: { _controller: MediaFilesBundle:Thumbs:index }
    requirements:
      path: .*

adm_media_thumbs_big:
  path: /media-big/{path}.{_format}
  defaults: { _controller: MediaFilesBundle:Thumbs:indexBig }
  requirements:
    path: .*
```

Add js files to gulp
```javascript
// app/gulp/assets/js/app.js
require('../../../../vendor/kisphp/media-files-bundle/assets/js/media-files').init();
require('../../../../vendor/kisphp/media-files-bundle/assets/js/media-files-download').init();
```

Add scss file to gulp
```scss
// app/gulp/assets/scss/main.scss
@import "../../../../vendor/kisphp/media-files-bundle/assets/scss/media-files";
```

Add copy icons to gulp
```javascript
// app/gulp/tasks/copy.js
gulp.task('copy-file-icons', function(){
  gulp.src([
    'vendor/kisphp/media-files-bundle/assets/file-icons/*.*'
  ], [{ base: './' }])
    .pipe(gulp.dest('web/file-icons'))
  ;
});

// at the end of file
GR.addTask('copy-file-icons');

```

Update translations:
```yaml
// src/AppBundle/Resources/translations/messages.en.yml
main_navigation:
  media_files: 'Media Files'

```

Add Navigation menu:
```php
// src/AppBundle/Services/KisphpAdminMenuManager.php
...
use MediaFilesBundle\Services\Menu\MediaFilesMenuItems;

class KisphpAdminMenuManager extends MenuManager
{
    /**
     * @return array
     */
    public function registerMenuItems()
    {
        return [
            ...
            MediaFilesMenuItems::class,
        ];
    }
}
```

Add the following commands in .gitlab-ci.yml in scripts_before
```bash
mkdir -p /tmp/_media_files/{alfa,beta}/{one,two}
touch /tmp/_media_files/alfa/one/file-1.jpg
touch /tmp/_media_files/alfa/file-2.jpg
```

Include bundle tests in codeception.yml file
```yaml
...
include:
    - vendor/kisphp/media-files-bundle
...
```

Add the following code to `app/gulp/assets/js/app.js`:
```javascript
const CopySelector = require('./modules/libs/copy-selector');

window.getMediaFileCode = function(element) {
  const tpl = '<lumas-img src="{0}{1}" alt="{2}"/>';

  let filePath = element.data('media-file-path');
  let fileAlt = element.data('media-file-alt');

  $('#media-snippet').val(tpl.formatString(mediaFilesUrl, filePath, fileAlt));

  $('#media-url').modal('show');
};

window.copyMediaFileSnippet = function() {
  let element = $('#media-snippet');

  CopySelector.copyElementText(element);
};
```

### Replace html templates

To chante a template in project level you may extend the components:

```html
// app/Resources/MediaFilesBundle/views/Default/modules/media-btns.html.twig
<div class="media-btns">
    <a href="#" class="btn-xs btn-danger">Delete</a>
</div>
```

### Change dropzone configuration parameters

To change dropzone configuration parameter extend the configuration file:

```html
// app/Resources/MediaFilesBundle/views/Default/upload-images-config-js.html.twig
<script>
  const dropzoneContainerId = '#my-dropzone';
  const dropzoneThumbnailId = '#thumb-tpl';
  const dropzoneMediaFilesListingId = '#media-files-listing';
  const dropzoneMediaRemoveElement = '.media-remove';

  const dropzoneUrl = $(dropzoneContainerId).data('action');
  const dropzoneFilesSize = 100; // 100MB
  const dropzoneUploadMaxFiles = 10; // upload 10 files at once 
  const dropzoneParallelUploads = 5; //  allow 5 files upload in the same time
  const dropzoneAutoProcessQueue = true;
</script>
```
