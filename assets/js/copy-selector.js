"use strict";

const CopySelector = new function() {
  this.copyElementText = (element) => {
    element.select();

    document.execCommand('copy');
    element.blur();

    this.colorizeElement(element);
  };

  this.colorizeElement = (element) => {
    element.addClass('greenify');
    setTimeout(() => {
      element.removeClass('greenify');
  }, 1000);
  };
};

module.exports = CopySelector;
