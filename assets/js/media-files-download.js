"use strict";

require('kisphp-format-string');

let KisphpAjax = require('../../../framework-admin-bundle/assets/tools/kisphp-ajax');
let KisphpAjaxCallbacks = require('../../../framework-admin-bundle/assets/tools/kisphp-callback');

KisphpAjax.downloadFile = function(url){

  let filesToDownload = url.split("\n");

  filesToDownload.forEach(function (item) {
    KisphpAjax.setUrl('/media/download').ajaxSubmit({
      url: item,
      path: $('#my-dropzone').data('path')
    }, 'showUploadedFile');
  });
};

KisphpAjaxCallbacks.showUploadedFile = function (response) {
  $('#download-file-input').val('');

  if (response.code !== 200) {
    swal("Error", response.message, "error");
  }

  if (response.code === 200) {
    var template = $('#thumb-tpl').html();

    $('#media-files-listing').prepend(template.formatString(response));

    // this code must be duplicated from vendor/kisphp/media-files-bundles/assets/js/modules/media-files.js
    $('.media-remove').click(window.mediaRemoveHandler);

    $('.get-code').click(function (e) {
      e.preventDefault();

      window.getMediaFileCode($(this));
    });

    $('#media-snippet-copy').click(function(e){
      e.preventDefault();

      window.copyMediaFileSnippet();
    });
  }

  $('#download-file-icon').addClass('fa-download').removeClass('fa-spinner fa-spin');
};

module.exports = {
  init: function() {

    $('#download-file-btn').click(function (e) {
      e.preventDefault();

      $('#download-file-icon').removeClass('fa-download').addClass('fa-spinner fa-spin');

      var urlToDownload = $('#download-file-input').val();

      KisphpAjax.downloadFile(urlToDownload);
    });

    $('#download-bulk-btn').click(function (e) {
      e.preventDefault();

      let input = $('#download-file-input')
      let textarea = $('<textarea></textarea>').attr({
        id: input.prop('id'),
        class: input.prop('class'),
        rows: 9,
        cols: 120,
        placeholder: input.prop('placeholder'),
      });
      textarea.text(input.val());
      input.after(textarea).remove();
    });

  }
};
