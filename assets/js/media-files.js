"use strict";

require('kisphp-format-string');

let KisphpAjax = require('../../../framework-admin-bundle/assets/tools/kisphp-ajax');
let KisphpAjaxCallbacks = require('../../../framework-admin-bundle/assets/tools/kisphp-callback');
let KisphpAlert = require('../../../framework-admin-bundle/assets/tools/kisphp-alert');

function createDirectoriesMapping(targets) {
  let directoriesArray = targets.split(',');

  // create first element to move back one directory
  let output = {'..': '-move to parent-'};
  for (const directory of directoriesArray) {
    // make sure empty directory is excluded
    if (!directory) {
      continue;
    }
    output[directory] = directory;
  }

  return output
}

KisphpAjax.deleteMediaFile = function(options, url){
  this.setUrl(url).ajaxSubmit(options, 'deleteCallback');
};

KisphpAjax.renameMediaFile = function(options, url){
  this.setUrl(url).ajaxSubmit(options, 'renameCallback');
};

KisphpAjax.moveFile = function(options, url){
  this.setUrl(url).ajaxSubmit(options, 'moveCallback');
};

KisphpAjax.changeViewType = function(options, url){
  this.setUrl(url).ajaxSubmit(options, function(){
    location.reload();
  });
};

KisphpAjaxCallbacks.moveCallback = function(ajaxResponse){
  Swal.hideLoading();
  Swal.close();

  if (ajaxResponse.code != 200) {
    console.log('something went wrong');
    console.log(ajaxResponse);

    Swal.fire({
      title: 'Error',
      text: 'Could not move the file',
      icon: 'error',
      timer: 4000
    });

    return;
  }

  $('#row-' + ajaxResponse.id).addClass('danger').fadeOut('slow');
};

KisphpAjaxCallbacks.renameCallback = function(ajaxResponse){
  Swal.hideLoading();
  Swal.close();

  if (ajaxResponse.code != 200) {
    Swal.fire({
      title: media_files.rename.title_error,
      text: media_files.rename.text_error,
      icon: 'error',
      timer: 4000
    });

    return;
  }

  $('#row-' + ajaxResponse.id).find('.thumb-title').html(ajaxResponse.filename);
  $('#row-' + ajaxResponse.id).find('.media-rename').hide();

  Swal.fire({
    title: media_files.rename.title_success,
    text: media_files.rename.text_success,
    icon: 'success',
    timer: 1000
  });
};

KisphpAjaxCallbacks.deleteCallback = function(ajaxResponse){
  if (parseInt(ajaxResponse.code) === 200) {
    $('#row-' + ajaxResponse.id).addClass('danger').fadeOut('slow');

    return null;
  }

  const tpl = "\n\nFileName: {0}\nFilePath: {1}\nFileTarget: {2}";

  let errorMessage = '' + ajaxResponse.message + tpl.formatString(
    ajaxResponse.debug_msg.fileName,
    ajaxResponse.debug_msg.filePath,
    ajaxResponse.debug_msg.fileTarget
  );

  KisphpAlert.error(errorMessage);
};

function executeFileRemoval(id, filePath, filename, deletePath) {
  KisphpAjax.deleteMediaFile({
    id: id,
    filePath: filePath,
    fileName: filename
  }, deletePath);
}

function executeFileRename(id, filepath, newName, renamePathUrl) {
  KisphpAjax.renameMediaFile({
    id: id,
    filepath: filepath,
    newName: newName
  }, renamePathUrl)
}

function executeFileMove(id, filepath, targetPath, movePathUrl) {
  KisphpAjax.moveFile({
    id: id,
    filepath: filepath,
    targetPath: targetPath
  }, movePathUrl)
}

function toggleConfirmation() {
  if (window.confirmFileDelete) {
    window.confirmFileDelete = false;
    $('#confirm-suppress').removeClass('fa-toggle-off text-default').addClass('fa-toggle-on text-success');
  } else {
    window.confirmFileDelete = true;
    $('#confirm-suppress').removeClass('fa-toggle-on text-success').addClass('fa-toggle-off text-default');
  }
}

function checkConfirmationStatus() {
  if (window.confirmFileDelete) {
    $('#confirm-suppress').removeClass('fa-toggle-on text-success').addClass('fa-toggle-off text-default');
  } else {
    $('#confirm-suppress').removeClass('fa-toggle-off text-default').addClass('fa-toggle-on text-success');
  }
}

function changeViewType(type, url) {
  KisphpAjax.changeViewType({
    type: type
  }, url);
}

window.confirmFileDelete = true

window.mediaRemoveHandler = function(e){
  e.preventDefault();

  const dropzoneElement = $('#my-dropzone');

  const filePath = dropzoneElement.data('path');
  const filename = $(this).data('filename');
  const id = $(this).data('id');
  const deletePath = dropzoneElement.data('delete-path');

  if (window.confirmFileDelete) {
    Swal.fire({
      title: trans.swal.remove.confirm.title,
      text: trans.swal.remove.confirm.text,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
    })
      .then(function(result) {
        if (result.isConfirmed) {
          executeFileRemoval(id, filePath, filename, deletePath);
        }
      });
  }
};

window.mediaRenameHandler = function(e){
  e.preventDefault();

  let filePath = $(this).data('path');
  let renamePathUrl = $(this).data('url');
  let elementId = $(this).data('id');
  let filename = $(this).data('filename');
  let elementIdFilename = $('#row-' + elementId).find('.thumb-title');
  let defaultValue = elementIdFilename.html().trim();

  Swal.fire({
    title: "Rename file",
    input: "text",
    inputAttributes: {
      autocapitalize: "off"
    },
    showCancelButton: true,
    inputValue: defaultValue,
    confirmButtonText: "Save",
    showLoaderOnConfirm: true
  }).then((result) => {
    if (result.isConfirmed) {
      executeFileRename(elementId, filePath, result.value, renamePathUrl);
      return;
    }

    Swal.fire({
      icon: 'error',
      title: 'Could not rename file',
      text: 'Something went wrong and the file was not renamed'
    });
  });
}

window.mediaMoveHandler = function(e){
  e.preventDefault();

  let filePath = $(this).data('path');
  let movePathUrl = $(this).data('url');
  let elementId = $(this).data('id');
  let targets = $(this).data('targets');

  Swal.fire({
    title: "Select a directory",
    input: 'select',
    inputOptions: createDirectoriesMapping(targets),
    inputPlaceholder: 'Select a target to move',
    showCancelButton: true,
  }).then((result) => {
    if (result.isConfirmed) {
      executeFileMove(elementId, filePath, result.value, movePathUrl);
    }
  });
}

module.exports = {
  init: () => {
    $('.media-remove').click(mediaRemoveHandler);
    $('.media-rename').click(mediaRenameHandler);
    $('.media-move').click(mediaMoveHandler);

    checkConfirmationStatus();

    $('#confirm-suppress-link').click(function(e){
      e.preventDefault();
      toggleConfirmation();
    });
    $('.media-files-view-type').click(function(e){
      e.preventDefault();
      let type = $(this).data('type');
      let url = $(this).data('url');
      changeViewType(type, url);
    });
  }
};
