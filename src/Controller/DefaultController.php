<?php

namespace MediaFilesBundle\Controller;

use MediaFilesBundle\Form\DirectoryForm;
use MediaFilesBundle\Services\MediaFilesManager;
use MediaFilesBundle\Transfer\CreateDirectoryTransfer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $path = $request->query->get('path', '');

        $filesManager = $this->get('media_files_manager');

        if ($request->cookies->has('media_files_display') === false) {
            $request->cookies->set('media_files_display', 'grid');
        }

        $directoryToScan = trim($filesManager->secureDirectoryPath($filesManager->getMediaDirectoryPath() . $path), '.');

        $mediaFilesExists = is_dir($directoryToScan);

        $filesCollection = $filesManager->getMediaFiles($directoryToScan, $this->getUser());

        $targetsToMove = [];
        /** @var \SplFileInfo $item */
        foreach ($filesCollection['directories'] as $item) {
            $targetsToMove[] = trim($item->getFilename());
        }
        sort($targetsToMove);

        return $this->render('MediaFilesBundle:Default:index.html.twig', [
            'allow_download_files' => (bool) ini_get('allow_url_fopen'),
            'path' => $path,
            'directories' => $filesCollection['directories'],
            'targets_to_move' => implode(',', $targetsToMove),
            'files' => $filesCollection['files'],
            'breadcrumb' => $filesManager->buildBreadcrumb($directoryToScan),
            'media_files_exists' => $mediaFilesExists,
            'allowed_images_extensions' => MediaFilesManager::getImagesExtensions(),
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $path = $request->query->get('path', '');
        $defaultData = new CreateDirectoryTransfer($path);

        $form = $this->createForm(DirectoryForm::class, $defaultData);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $filesManager = $this->get('media_files_manager');

            $filesManager->createDirectory($form->getData());

            return $this->redirect(
                $this->generateUrl('adm_media', ['path' => $path])
            );
        }

        return $this->render('@FrameworkAdmin/components/form.html.twig', [
            'section' => [
                'list_path' => 'adm_media',
                'add_path' => '',
            ],
            'id' => null,
            'form' => $form->createView(),
            'section_title' => 'Create directory',
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function viewTypeAction(Request $request)
    {
        $type = $request->request->get('type', 'grid');

        $response = new JsonResponse([
            'code' => 200,
            'type' => $type,
        ]);

        $response->headers->setCookie(new Cookie('media_files_display', $type));

        return $response;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function downloadFileAction(Request $request)
    {
        $path = $request->query->get('path');

        $filePath = str_replace(['.../', '../', './'], '', $this->getParameter('media_files_path') . '/' . $path);

        if (is_file($filePath) === false) {
            throw new NotFoundHttpException();
        }

        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-Type', mime_content_type($filePath));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($filePath) . '";');
        $response->headers->set('Content-Length: ', filesize($filePath));

        $response->sendHeaders();

        $response->setContent(file_get_contents($filePath));

        return $response;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $id = $request->request->get('id');
        $filePath = $request->request->get('filePath');
        $fileName = $request->request->get('fileName');

        $filesManager = $this->get('media_files_manager');

        $deleted = $filesManager->deleteFile($filesManager->secureDirectoryPath($filePath), $fileName);
        $deleted['id'] = $id;

        return new JsonResponse($deleted);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function renameAction(Request $request)
    {
        $post = $request->request->all();

        $id = $post['id'];
        $filePath = $post['filepath'];
        $newName = $post['newName'];

        $mediaFilesRootDir = $this->getParameter('media_files_path');

        $file = new \SplFileInfo($mediaFilesRootDir . '/' . $filePath);

        $manager = $this->get('media_files_manager');

        $renamed = $manager->renameFile($file, $newName);
        $renamed['id'] = $id;
        $renamed['filename'] = $newName;

        return new JsonResponse($renamed);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function moveAction(Request $request)
    {
        $post = $request->request->all();

        $id = $post['id'];
        $filePath = $post['filepath'];
        $targetPath = $post['targetPath'];

        $mediaFilesRootDir = $this->getParameter('media_files_path');

        $file = new \SplFileInfo($mediaFilesRootDir . '/' . $filePath);

        $manager = $this->get('media_files_manager');

        $renamed = $manager->moveFile($file, $file->getPath() . '/' . $targetPath . '/' . $file->getFilename());
        $renamed['id'] = $id;
        $renamed['filename'] = $file->getPath() . '/' . $targetPath;

        return new JsonResponse($renamed);
    }
}
