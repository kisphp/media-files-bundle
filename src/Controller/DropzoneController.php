<?php

namespace MediaFilesBundle\Controller;

use MediaFilesBundle\Exceptions\DuplicateUploadedFile;
use MediaFilesBundle\Transfer\MediaFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DropzoneController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function uploadAction(Request $request)
    {
        $path = $request->query->get('path', '');
        $path = trim($path);
        $mediaFilesManager = $this->get('media_files_manager');

        try {
            $uploadedImage = $mediaFilesManager->uploadImages($request, $path);
        } catch (DuplicateUploadedFile $e) {
            return new JsonResponse([
                'code' => 409,
                'message' => $e->getMessage(),
            ]);
        }

        $newFileLocation = $uploadedImage->getDirectory() . '/' . $uploadedImage->getFilename();

        $fileInfo = new \SplFileInfo($newFileLocation);

        $relativeMediaPathWithoutExtension = $mediaFilesManager->calculateRelativeMediaPath($fileInfo, $path);

        $response = $mediaFilesManager->buildUploadedFileResponse($uploadedImage, $fileInfo, $path);

        $response['media_url'] = $this->generateUrl('adm_media_thumbs', [
            'path' => $relativeMediaPathWithoutExtension,
            '_format' => $fileInfo->getExtension(),
        ]);

        return new JsonResponse($response);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function downloadAction(Request $request)
    {
        $path = $request->request->get('path', '');
        $path = trim($path);
        $mediaFilesManager = $this->get('media_files_manager');

        try {
            $uploadedImage = $mediaFilesManager->downloadImage($request->request->get('url'), $path);
        } catch (DuplicateUploadedFile $e) {
            return new JsonResponse([
                'code' => 409,
                'message' => $e->getMessage(),
            ]);
        }

        $relativeMediaPathWithoutExtension = $mediaFilesManager->calculateRelativeMediaPath($uploadedImage, $path);

        $mediaFile = new MediaFile();
        $mediaFile->setFilename($uploadedImage->getFilename());
        $mediaFile->setDirectory($uploadedImage->getPathname());

        $response = $mediaFilesManager->buildUploadedFileResponse($mediaFile, $uploadedImage, $path);

        $response['media_url'] = $this->generateUrl('adm_media_thumbs', [
            'path' => $relativeMediaPathWithoutExtension,
            '_format' => $uploadedImage->getExtension(),
        ]);

        return new JsonResponse($response);
    }
}
