<?php

namespace MediaFilesBundle\Controller;

use Kisphp\ImageResizer;
use MediaFilesBundle\Services\MediaFilesManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ThumbsController extends Controller
{
    const IMG_WIDTH = 192;
    const IMG_HEIGHT = 160;

    /**
     * @param string $path
     * @param string $_format
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Kisphp\ImageFileTypeNotAllowed
     */
    public function indexAction($path, $_format)
    {
        $thumbPath = dirname($this->get('kernel')->getRootDir()) . '/web/';

        $cachePath = $thumbPath . 'media-thumbs/' . $path . '.' . $_format;

        $thumbFileDirectory = dirname($cachePath);

        if (!is_dir($thumbFileDirectory)) {
            mkdir($thumbFileDirectory, 0775, true);
        }

        $sourcePath = $this->getParameter('media_files_path') . '/' . $path . '.' . $_format;

        $sourceFileExists = true;
        if (is_file($sourcePath) === false || in_array($_format, MediaFilesManager::getImagesExtensions(), true) === false) {
            // Skip thumbnail create when source image was not found
            $sourceFileExists = false;

            $sourcePath = $thumbPath . '/file-icons/' . $_format . '.png';
            if (is_file($sourcePath) === false) {
                $sourcePath = $thumbPath . '/file-icons/ext.png';
            }
        }

        $img = new ImageResizer(100);
        $img->load($sourcePath);
        $img->setTarget($cachePath);
        $img->resize(self::IMG_WIDTH, self::IMG_HEIGHT);

        $imgContent = $img->display($sourceFileExists);

        return new Response($imgContent);
    }

    /**
     * @param string $path
     * @param string $_format
     *
     * @return Response
     *
     * @throws \Kisphp\ImageFileTypeNotAllowed
     */
    public function indexBigAction($path, $_format)
    {
        $thumbPath = dirname($this->get('kernel')->getRootDir()) . '/web/';

        $cachePath = $thumbPath . 'media-big/' . $path . '.' . $_format;

        $thumbFileDirectory = dirname($cachePath);

        if (!is_dir($thumbFileDirectory)) {
            mkdir($thumbFileDirectory, 0775, true);
        }

        $sourcePath = $this->getParameter('media_files_path') . '/' . $path . '.' . $_format;

        $sourceFileExists = true;
        if (is_file($sourcePath) === false || in_array($_format, MediaFilesManager::getImagesExtensions(), true) === false) {
            $sourceFileExists = false;

            $sourcePath = $thumbPath . '/file-icons/' . $_format . '.png';
            if (is_file($sourcePath) === false) {
                $sourcePath = $thumbPath . '/file-icons/ext.png';
            }
        }

        $img = new ImageResizer(100);
        $img->load($sourcePath);
        $img->setTarget($cachePath);
        $img->resize(1000, 0);

        $imgContent = $img->display($sourceFileExists);

        return new Response($imgContent);
    }
}
