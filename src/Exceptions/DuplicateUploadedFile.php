<?php

namespace MediaFilesBundle\Exceptions;

class DuplicateUploadedFile extends \Exception
{
    protected $message = 'File already exists';
}
