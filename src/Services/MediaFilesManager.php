<?php

namespace MediaFilesBundle\Services;

use Kisphp\Entity\FileInterface;
use Kisphp\Utils\Files;
use MediaFilesBundle\Exceptions\DuplicateUploadedFile;
use MediaFilesBundle\Transfer\CreateDirectoryTransfer;
use MediaFilesBundle\Transfer\MediaFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;

class MediaFilesManager
{
    /**
     * @var string
     */
    protected $mediaFilesRootDirectory;

    /**
     * @var string
     */
    protected $uploadDirectory;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var bool
     */
    protected $allowMediaFilesOverride = false;

    /**
     * @param string $mediaFilesRootDirectory
     * @param false $allowMediaFilesOverride
     */
    public function __construct($mediaFilesRootDirectory, RequestStack $request, $allowMediaFilesOverride = false)
    {
        $this->mediaFilesRootDirectory = $mediaFilesRootDirectory;
        $this->allowMediaFilesOverride = (bool) $allowMediaFilesOverride;
        $this->request = $request->getMasterRequest();
    }

    /**
     * @return string[]
     */
    public static function getImagesExtensions(): array
    {
        return [
            'jpg',
            'jpeg',
            'gif',
            'png',
            'webp',
        ];
    }

    /**
     * @return string
     */
    public function getMediaFilesRootDirectory()
    {
        return $this->mediaFilesRootDirectory;
    }

    /**
     * @param string $mediaFilesRootDirectory
     */
    public function setMediaFilesRootDirectory($mediaFilesRootDirectory)
    {
        $this->mediaFilesRootDirectory = $mediaFilesRootDirectory;
    }

    /**
     * @param string $path
     *
     * @return array
     */
    public function buildUploadedFileResponse(MediaFile $uploadedImage, \SplFileInfo $fileInfo, $path)
    {
        $path = $this->fixPath($path);

        return [
            'code' => 200,
            'id' => md5($uploadedImage->getFilename()),
            'path' => $path,
            'filename' => $uploadedImage->getFilename(),
            'image_size' => Files::fileSize($fileInfo->getSize()),
            'extension' => $fileInfo->getExtension(),
            'relative_media_path' => $this->calculateRelativeMediaPath($fileInfo, $path),
        ];
    }

    /**
     * relative media path without extension.
     *
     * @param string $path
     *
     * @return string
     */
    public function calculateRelativeMediaPath(\SplFileInfo $fileInfo, $path)
    {
        $items = explode('.', $fileInfo->getFilename());
        $relativeMediaPath = $this->fixPath($path) . '/' . $items[0];

        return substr($relativeMediaPath, 1);
    }

    public function createDirectory(CreateDirectoryTransfer $formData)
    {
        $path = $this->secureDirectoryPath($this->mediaFilesRootDirectory . '/' . $formData->getPath()) . '/' . $formData->getName();

        if (!is_dir($path)) {
            return mkdir($path, 0775, true);
        }

        return true;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function secureDirectoryPath($path)
    {
        $path = str_replace(['../', './'], '', $path);
        $path = preg_replace('/([\/]{2,})/', '/', $path);

        return rtrim($path, '/.');
    }

    /**
     * @param string $currentPath
     *
     * @return array
     */
    public function buildBreadcrumb($currentPath)
    {
        $currentPath = str_replace($this->getMediaDirectoryPath(), '', $currentPath);

        $currentPath = $this->secureDirectoryPath($currentPath);

        $items = explode('/', $currentPath);

        $breadcrumb = [];
        foreach ($items as $index => $value) {
            if ($value === '.' || empty($value)) {
                continue;
            }
            $breadcrumb[] = [
                'name' => $value,
                'path' => implode('/', array_slice($items, 0, $index + 1)),
            ];
        }

        return $breadcrumb;
    }

    /**
     * @param string $directoryToScan
     *
     * @return array
     */
    public function getMediaFiles($directoryToScan, UserInterface $user = null)
    {
        if (is_dir($directoryToScan) === false) {
            return [
                'directories' => [],
                'files' => [],
            ];
        }

        $unsortedList = [];
        foreach (new \DirectoryIterator($directoryToScan) as $item) {
            if ($item->isDot()) {
                continue;
            }
            $unsortedList[] = $item->getFileInfo()->getPathname();
        }

        asort($unsortedList);

        $filesSorted = [];
        $directories = [];
        foreach ($unsortedList as $file) {
            $element = new \SplFileInfo($file);

            if ($element->getFilename() === 'ai-cache') {
                continue;
            }

            if ($element->isDir()) {
                if (
                    strpos($element->getBasename(), '.') === 0
                    && (
                        $this->request->query->has('h') === false
                        || !in_array('ROLE_SUPER_ADMIN', $user->getRoles(), true)
                    )
                ) {
                    continue;
                }
                $directories[] = $element;

                continue;
            }

            $filesSorted[] = $element;
        }
        unset($unsortedList);

        return [
            'directories' => $directories,
            'files' => $filesSorted,
        ];
    }

    /**
     * @param string $filePath
     * @param string $fileName
     *
     * @return array
     */
    public function deleteFile($filePath, $fileName)
    {
        $this->uploadDirectory = $filePath;

        $fileTarget = $this->secureDirectoryPath($this->getTargetUploadDirectory() . '/' . $fileName);

        $debug = [
            'filePath' => $filePath,
            'fileName' => $fileName,
            'fileTarget' => $fileTarget,
        ];

        if (is_file($fileTarget)) {
            unlink($fileTarget);

            return $this->createDeleteOutputArray(200, 'Success', $debug);
        }

        if (is_dir($fileTarget)) {
            $filesInTarget = scandir($fileTarget, SCANDIR_SORT_NONE);
            if (count($filesInTarget) > 2) {
                return $this->createDeleteOutputArray(500, 'You can delete only empty directories !', $debug);
            }

            rmdir($fileTarget);

            return $this->createDeleteOutputArray(200, 'Success', $debug);
        }

        return $this->createDeleteOutputArray(404, 'File not found', $debug);
    }

    /**
     * @param string $newName
     *
     * @return array
     */
    public function renameFile(\SplFileInfo $fileInfo, $newName)
    {
        $sourcePath = $fileInfo->getPathname();
        $targetPath = $fileInfo->getPath() . '/' . $newName;

        try {
            $renamed = rename($sourcePath, $targetPath);
        } catch (\Exception $e) {
            $renamed = false;
        }

        if ($renamed === false) {
            return [
                'code' => 500,
                'message' => 'The file could not be renamed',
            ];
        }

        return [
            'code' => 200,
            'message' => 'File was successfully renamed',
        ];
    }

    /**
     * @param string $targetPath
     *
     * @return array
     */
    public function moveFile(\SplFileInfo $fileInfo, $targetPath)
    {
        try {
            $renamed = rename($fileInfo->getPathname(), $targetPath);
        } catch (\Exception $e) {
            $renamed = false;
        }

        if ($renamed === false) {
            return [
                'code' => 500,
                'message' => 'The file could not be moved',
            ];
        }

        return [
            'code' => 200,
            'message' => 'File was successfully moved',
        ];
    }

    /**
     * @param string $path
     *
     * @return \Kisphp\Entity\FileInterface
     *
     * @throws \MediaFilesBundle\Exceptions\DuplicateUploadedFile
     */
    public function uploadImages(Request $request, $path)
    {
        $this->uploadDirectory = $path;

        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
        $file = $request->files->get('file');

        // @var MediaFile $image
        return $this->makeUpload($file, $this->createEntity());
    }

    /**
     * Download file from url and save it on local directory.
     *
     * @param string $url
     * @param string $pathToSave
     *
     * @return \SplFileInfo
     *
     * @throws \MediaFilesBundle\Exceptions\DuplicateUploadedFile
     */
    public function downloadImage($url, $pathToSave)
    {
        $this->uploadDirectory = $pathToSave;

        $tmpFilePath = '/tmp/' . uniqid('media-', false);
        $targetFile = $this->secureDirectoryPath($this->getTargetUploadDirectory()) . '/' . $this->generateUploadedFilename($url);

        if (is_file($targetFile)) {
            throw new DuplicateUploadedFile();
        }

        $content = file_get_contents($url);
        file_put_contents($tmpFilePath, $content);
        copy($tmpFilePath, $targetFile);
        unlink($tmpFilePath);

        return new \SplFileInfo($targetFile);
    }

    /**
     * @return string
     */
    public function getMediaDirectoryPath()
    {
        return $this->mediaFilesRootDirectory;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function fixPath($path)
    {
        $path = trim($path);
        if ($path === '.' || $path === '/' || $path === '') {
            return '';
        }

        return str_replace('//', '/', '/' . $path);
    }

    /**
     * @param string $url
     *
     * @return string
     */
    protected function generateUploadedFilename($url)
    {
        $filename = basename($url);
        $filename = strtolower($filename);
        // remove url parameters from the filename
        $fileParameters = explode('?', $filename);
        $filename = $fileParameters[0];

        return str_replace([' ', '_'], '-', $filename);
    }

    /**
     * @param string $code
     * @param string $message
     * @param array $debug
     *
     * @return array
     */
    protected function createDeleteOutputArray($code, $message, $debug)
    {
        return [
            'code' => $code,
            'message' => $message,
            'debug_msg' => $debug,
        ];
    }

    /**
     * @return \Kisphp\Entity\FileInterface
     *
     * @throws \MediaFilesBundle\Exceptions\DuplicateUploadedFile
     */
    protected function makeUpload(\SplFileInfo $uploadedFile, FileInterface $fileInterface)
    {
        $uploadDirectory = $this->getTargetUploadDirectory();

        $fileInterface->setDirectory($uploadDirectory);
        $fileInterface->setFilename($uploadedFile->getClientOriginalName());
        $fileInterface->setTitle($uploadedFile->getClientOriginalName());

        $this->saveFileOnDisk($uploadedFile, $fileInterface);

        return $fileInterface;
    }

    /**
     * @return bool|string
     */
    protected function getTargetUploadDirectory()
    {
        return $this->mediaFilesRootDirectory . $this->uploadDirectory;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File
     *
     * @throws \MediaFilesBundle\Exceptions\DuplicateUploadedFile
     */
    protected function saveFileOnDisk(\SplFileInfo $uploadedFile, FileInterface $fileInterface)
    {
        if (is_file($this->getTargetUploadDirectory() . '/' . $fileInterface->getFilename()) && $this->allowMediaFilesOverride === false) {
            throw new DuplicateUploadedFile();
        }

        return $uploadedFile->move($this->getTargetUploadDirectory(), $fileInterface->getFilename());
    }

    /**
     * @return \MediaFilesBundle\Transfer\MediaFile
     */
    protected function createEntity()
    {
        return new MediaFile();
    }
}
