<?php

namespace MediaFilesBundle\Services\Menu;

use Kisphp\FrameworkAdminBundle\Services\MenuItem;

class MediaFilesMenuItems extends MenuItem
{
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => true,
            'valid_feature' => 'media',
            'label' => 'main_navigation.media_files',
        ]);
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'media',
            'match_route' => 'media',
            'path' => 'adm_media',
            'icon' => 'fa-image',
            'label' => 'main_navigation.media_files',
        ]);
    }
}
