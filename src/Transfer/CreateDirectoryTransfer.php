<?php

namespace MediaFilesBundle\Transfer;

use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

class CreateDirectoryTransfer
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $path = '';

    public function __construct(string $dirpath)
    {
        $this->path = $dirpath;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('name', [
            new NotBlank(),
            new Callback(function ($value, ExecutionContextInterface $context) {
                $filteredText = preg_replace('/([^a-z0-9\-\_\.\/]+)/', '', $value);
                if (strcmp($value, $filteredText) !== 0) {
                    $context->buildViolation('Directory name must be lowercase in this format: a-z0-9.-_/')
                        ->atPath('name')
                        ->addViolation()
                    ;
                }
            }),
        ]);
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string) $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
