<?php

namespace MediaFilesBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;
use Kisphp\Utils\Files;

class FileSize extends AbstractTwigFilter
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'fileSize';
    }

    /**
     * @return callable|\Closure
     */
    protected function getExtensionCallback()
    {
        return function ($fileSize) {
            return Files::fileSize($fileSize);
        };
    }
}
