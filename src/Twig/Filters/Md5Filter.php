<?php

namespace MediaFilesBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;

class Md5Filter extends AbstractTwigFilter
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'md5';
    }

    /**
     * @return callable|\Closure
     */
    protected function getExtensionCallback()
    {
        return function ($string) {
            return md5($string);
        };
    }
}
