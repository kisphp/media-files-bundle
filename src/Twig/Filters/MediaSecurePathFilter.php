<?php

namespace MediaFilesBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;

class MediaSecurePathFilter extends AbstractTwigFilter
{
    /**
     * @var string
     */
    protected $mediaFilesDirectory;

    /**
     * @param string $mediaFilesDirectory
     */
    public function __construct($mediaFilesDirectory)
    {
        $this->mediaFilesDirectory = $mediaFilesDirectory;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'mediaSecurePath';
    }

    /**
     * @return callable|\Closure
     */
    protected function getExtensionCallback()
    {
        return function ($variable) {
            return str_replace($this->mediaFilesDirectory, '', $variable);
        };
    }
}
