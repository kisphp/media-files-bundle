<?php

namespace MediaFilesBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;

class RelativeMediaPathFilter extends AbstractTwigFilter
{
    /**
     * @var
     */
    protected $mediaFilesPath;

    /**
     * @param $mediaFilesPath
     */
    public function __construct($mediaFilesPath)
    {
        $this->mediaFilesPath = $mediaFilesPath;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'relativeMediaPath';
    }

    /**
     * @return callable|\Closure
     */
    protected function getExtensionCallback()
    {
        return function ($fullPath) {
            $path = str_replace($this->mediaFilesPath, '', $fullPath);

            if (strpos($path, '/') === 0) {
                $path = substr($path, 1);
            }

            return $path;
        };
    }
}
