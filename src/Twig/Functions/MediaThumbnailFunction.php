<?php

namespace MediaFilesBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;

class MediaThumbnailFunction extends AbstractTwigFunction
{
    /**
     * @var string
     */
    protected $rootDir;

    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'media_thumbnail';
    }

    /**
     * @return callable|\Closure
     */
    protected function getExtensionCallback()
    {
        return function (\SplFileInfo $file) {
            $path = str_replace($this->rootDir, '', $file->getPathname());

            if (strpos($path, '/') !== 0) {
                $path = '/' . $path;
            }

            return '/media-thumbs' . $path;
        };
    }
}
