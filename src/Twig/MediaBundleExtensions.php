<?php

namespace MediaFilesBundle\Twig;

use MediaFilesBundle\Twig\Filters\FileSize;
use MediaFilesBundle\Twig\Filters\Md5Filter;
use MediaFilesBundle\Twig\Filters\MediaSecurePathFilter;
use MediaFilesBundle\Twig\Filters\RelativeMediaPathFilter;
use MediaFilesBundle\Twig\Functions\MediaThumbnailBigFunction;
use MediaFilesBundle\Twig\Functions\MediaThumbnailFunction;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MediaBundleExtensions extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            $this->createMediaSecurePathFilter(),
            $this->createRelativeMediaPathFilter(),
            Md5Filter::create(),
            FileSize::create(),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            $this->createMediaThumbnailFunction(),
            $this->createMediaThumbnailBigFunction(),
        ];
    }

    /**
     * @return \Twig_SimpleFunction
     */
    protected function createRelativeMediaPathFilter()
    {
        $filter = new RelativeMediaPathFilter($this->container->getParameter('media_files_path'));

        return $filter->getExtension();
    }

    /**
     * @return \Twig_SimpleFunction
     */
    protected function createMediaSecurePathFilter()
    {
        $filter = new MediaSecurePathFilter($this->container->getParameter('media_files_path'));

        return $filter->getExtension();
    }

    /**
     * @return \Twig_SimpleFunction
     */
    protected function createMediaThumbnailFunction()
    {
        $func = new MediaThumbnailFunction($this->container->getParameter('media_files_path'));

        return $func->getExtension();
    }

    /**
     * @return \Twig_SimpleFunction
     */
    protected function createMediaThumbnailBigFunction()
    {
        $func = new MediaThumbnailBigFunction($this->container->getParameter('media_files_path'));

        return $func->getExtension();
    }
}
