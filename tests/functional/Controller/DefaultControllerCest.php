<?php

namespace Functional\MediaFilesBundle\Controller;

use MediaFilesBundle\FunctionalTester;

/**
 * @group media
 */
class DefaultControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function media_files_page(\FunctionalTester $i)
    {
        $i->wantTo('test page /media');
        $i->amOnPage('/media/');
        $i->see('Media Files', 'h3');
        $i->see('Root');
        $i->see('alfa');
        $i->see('beta');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function media_files_in_path_page(\FunctionalTester $i)
    {
        $i->amOnPage('/media/');
        $i->see('Media Files', 'h3');
        $i->see('Root');
        $i->see('alfa');
        $i->see('beta');
        $i->canSeeResponseCodeIs(200);

        $i->click('alfa');
        $i->canSeeResponseCodeIs(200);
        $i->canSeeCurrentUrlMatches('/alfa/');
        $i->see('one');
        $i->see('file-2.jpg');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function create_dir_page(\FunctionalTester $i)
    {
        $i->amOnPage('/media/create-dir?path=.');
        $i->see('Create Directory');
        $i->canSeeResponseCodeIs(200);
    }

    /**
     * @param \FunctionalTester $i
     */
    public function create_dir_page_no_filling(\FunctionalTester $i)
    {
        $i->amOnPage('/media/create-dir?path=.');
        $i->see('Create Directory');
        $i->click('Save', 'input.btn');
        $i->canSeeResponseCodeIs(200);
        $i->canSee('This Value should not be blank');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function create_dir_page_not_allowed_characters(\FunctionalTester $i)
    {
        $i->amOnPage('/media/create-dir?path=.');
        $i->see('Create Directory');
        $i->fillField('directory_form[name]', 'Alfa Beta');
        $i->click('Save', 'input.btn');
        $i->canSeeResponseCodeIs(200);
        $i->canSee('Directory name must be');
    }

    /**
     * @param \FunctionalTester $i
     */
    public function create_dir_page_allowed_characters(\FunctionalTester $i)
    {
        $i->amOnPage('/media/create-dir?path=.');
        $i->see('Create Directory');
        $i->fillField('directory_form[name]', 'alfa1beta/one.two-three_four');
        $i->click('Save', 'input.btn');
        $i->canSeeResponseCodeIs(200);
        $i->cantSee('Directory name must be');
    }

    public function download_uploaded_file(FunctionalTester $i)
    {
        touch('/tmp/_media_files/file-1.jpg');
        $i->amOnPage('/media/download-file?path=file-1.jpg');
        $i->seeResponseCodeIs(200);
    }

    public function download_uploaded_file_from_directory(FunctionalTester $i)
    {
        if (!is_dir('/tmp/_media_files/uploaded')) {
            mkdir('/tmp/_media_files/uploaded', 0755, true);
        }
        touch('/tmp/_media_files/uploaded/file-1.jpg');
        $i->amOnPage('/media/download-file?path=uploaded/file-1.jpg');
        $i->seeResponseCodeIs(200);
    }

    public function download_uploaded_file_not_found(FunctionalTester $i)
    {
        $i->amOnPage('/media/download-file?path=not/found/file-1.jpg');
        $i->seeResponseCodeIs(404);
    }

    public function download_uploaded_file_not_found_system(FunctionalTester $i)
    {
        $i->amOnPage('/media/download-file?path=../../../etc/passwd');
        $i->seeResponseCodeIs(404);
    }

    public function view_grid_or_table(FunctionalTester $i)
    {
        $i->amOnPage('/');
        $i->sendAjaxPostRequest('/media/view-type', [
            'type' => 'grid',
        ]);

        $i->seeCookie('media_files_display');
    }

    public function rename_media_file(FunctionalTester $i)
    {
        $i->amOnPage('/media/');
        $i->see('file-3.jpg');

        $i->sendAjaxPostRequest('/media/rename', [
            'id' => md5('file-3.jpg'),
            'filepath' => 'file-3.jpg',
            'newName' => 'file-4.jpg',
        ]);

        $i->amOnPage('/media/');
        $i->see('file-4.jpg');
    }
}
