<?php

namespace Functional\MediaFilesBundle\Controller;

/**
 * @group thumbs
 */
class ThumbsControllerCest
{
    public function test_thumbnails(\FunctionalTester $i)
    {
        copy(codecept_data_dir() . '/blank.jpg', '/tmp/_media_files/alfa/image.jpg');

        $i->amOnPage('/media-thumbs/alfa/image.jpg');
        $i->seeResponseCodeIs(200);
    }

    public function test_thumbnails_no_file(\FunctionalTester $i)
    {
        $i->amOnPage('/media-thumbs/epsilon/image.jpg');
        $i->seeResponseCodeIs(200);
    }

    public function test_thumbnails_no_file_extension(\FunctionalTester $i)
    {
        $i->amOnPage('/media-thumbs/gama/image.php4');
        $i->seeResponseCodeIs(200);
    }

    public function test_big_thumbnails(\FunctionalTester $i)
    {
        copy(codecept_data_dir() . '/blank.jpg', '/tmp/_media_files/alfa/image.jpg');

        $i->amOnPage('/media-big/alfa/image.jpg');
        $i->seeResponseCodeIs(200);
    }

    public function test_big_thumbnails_no_file(\FunctionalTester $i)
    {
        $i->amOnPage('/media-big/epsilon/image.jpg');
        $i->seeResponseCodeIs(200);
    }

    public function test_big_thumbnails_no_file_extension(\FunctionalTester $i)
    {
        $i->amOnPage('/media-big/gama/image.php4');
        $i->seeResponseCodeIs(200);
    }
}
