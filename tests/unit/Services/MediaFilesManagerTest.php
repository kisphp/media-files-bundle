<?php

namespace Unit\MediaFilesBundle\Services;

use Codeception\Test\Unit;
use MediaFilesBundle\Exceptions\DuplicateUploadedFile;
use MediaFilesBundle\Services\MediaFilesManager;
use MediaFilesBundle\Transfer\CreateDirectoryTransfer;
use MediaFilesBundle\Transfer\MediaFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @group media
 */
class MediaFilesManagerTest extends Unit
{
    const MEDIA_FILES_ROOT_PATH = '/tmp/_media_files/';

    const TEST_IMAGE_FILE = '/tmp/test-image.jpg';

    public function test_createDirectory_in_root()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('.');
        $transfer->setName('directory-1');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/directory-1'));
    }

    public function test_createDirectory_in_alfa_with_dot()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('./alfa');
        $transfer->setName('.directory-2');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/alfa/.directory-2'));

        exec('rm -rf ' . self::MEDIA_FILES_ROOT_PATH . '/alfa/.directory-2');
    }

    public function test_createDirectory_in_beta_without_dot()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/beta');
        $transfer->setName('directory-3');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/beta/directory-3'));
    }

    public function test_createDirectory_tree()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/delta');
        $transfer->setName('one/two/three');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/delta/one'));
        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/delta/one/two'));
        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/delta/one/two/three'));
    }

    public function test_deleteDirectory()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/to-delete');
        $transfer->setName('one');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/one'));

        $delete = $manager->deleteFile('to-delete', 'one');

        $this->assertFalse(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/one'));

        $this->assertSame('Success', $delete['message']);
        $this->assertSame(200, $delete['code']);
    }

    public function test_createExistingDirectory()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('.');
        $transfer->setName('alfa-recreated');

        $manager->createDirectory($transfer);
        $created = $manager->createDirectory($transfer);

        $this->assertTrue($created);
    }

    public function test_deleteDirectory_with_dot()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/to-delete');
        $transfer->setName('two');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/two'));

        $delete = $manager->deleteFile('to-delete', './two');

        $this->assertFalse(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/two'));

        $this->assertSame('Success', $delete['message']);
        $this->assertSame(200, $delete['code']);
    }

    public function test_deleteDirectory_from_dot_path()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/to-delete');
        $transfer->setName('two');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/two'));

        $delete = $manager->deleteFile('to-delete', './two');

        $this->assertFalse(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/two'));

        $this->assertSame('Success', $delete['message']);
        $this->assertSame(200, $delete['code']);
    }

    public function test_deleteDirectory_with_slash()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/to-delete');
        $transfer->setName('three');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/three'));

        $delete = $manager->deleteFile('to-delete', '/three');

        $this->assertFalse(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/three'));

        $this->assertSame('Success', $delete['message']);
        $this->assertSame(200, $delete['code']);
    }

    public function test_deleteFile()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/to-delete');
        $transfer->setName('four');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/four'));

        touch(self::MEDIA_FILES_ROOT_PATH . '/to-delete/four/file.txt');

        $delete = $manager->deleteFile('to-delete/four', 'file.txt');

        $this->assertFalse(is_file(self::MEDIA_FILES_ROOT_PATH . '/to-delete/four/file.txt'), 'Could not delete file.txt');

        $this->assertSame('Success', $delete['message']);
        $this->assertSame(200, $delete['code']);
    }

    public function test_deleteFile_not_existant()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/to-delete');
        $transfer->setName('five');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/five'));

        $delete = $manager->deleteFile('to-delete/five', '/file.txt');

        $this->assertFalse(is_file(self::MEDIA_FILES_ROOT_PATH . '/to-delete/five/file.txt'), 'Could not delete file.txt');

        $this->assertSame('File not found', $delete['message']);
        $this->assertSame(404, $delete['code']);
    }

    public function test_deleteDirectory_not_empty()
    {
        $manager = $this->createManager();

        $transfer = new CreateDirectoryTransfer('/to-delete');
        $transfer->setName('six/seven');

        $manager->createDirectory($transfer);

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/six'));
        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/six/seven'));

        $delete = $manager->deleteFile('to-delete', '/six');

        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/six'));
        $this->assertTrue(is_dir(self::MEDIA_FILES_ROOT_PATH . '/to-delete/six/seven'));

        $this->assertSame('You can delete only empty directories !', $delete['message']);
        $this->assertSame(500, $delete['code']);
    }

    /**
     * @group md
     */
    public function test_build_upload_response_path_1()
    {
        touch(self::TEST_IMAGE_FILE);

        $path = 'alfa';
        $uploadImage = new MediaFile();
        $uploadImage->setDirectory($path);
        $uploadImage->setFilename('test-image.jpg');
        $fileInfo = new \SplFileInfo(self::TEST_IMAGE_FILE);

        $manager = $this->createManager();

        $resp = $manager->buildUploadedFileResponse($uploadImage, $fileInfo, $path);

        $this->assertSame(200, $resp['code']);
        $this->assertSame('/alfa', $resp['path']);
        $this->assertSame('test-image.jpg', $resp['filename']);
        $this->assertSame('alfa/test-image', $resp['relative_media_path']);

        unlink(self::TEST_IMAGE_FILE);
    }

    /**
     * @group md
     */
    public function test_build_upload_response_path_2()
    {
        touch(self::TEST_IMAGE_FILE);

        $path = '/alfa';
        $uploadImage = new MediaFile();
        $uploadImage->setDirectory($path);
        $uploadImage->setFilename('test-image.jpg');
        $uploadImage->setId(1);
        $uploadImage->setTitle('demo');
        $fileInfo = new \SplFileInfo(self::TEST_IMAGE_FILE);

        $manager = $this->createManager();

        $resp = $manager->buildUploadedFileResponse($uploadImage, $fileInfo, $path);

        $this->assertSame(200, $resp['code']);
        $this->assertSame('/alfa', $resp['path']);
        $this->assertSame('test-image.jpg', $resp['filename']);
        $this->assertSame('alfa/test-image', $resp['relative_media_path']);
        $this->assertSame($path, $uploadImage->getDirectory());
        $this->assertNotNull($uploadImage->getId());
        $this->assertSame('demo', $uploadImage->getTitle());

        unlink(self::TEST_IMAGE_FILE);
    }

    /**
     * @group md
     */
    public function test_build_upload_response_path_3()
    {
        touch(self::TEST_IMAGE_FILE);

        $path = '';
        $uploadImage = new MediaFile();
        $uploadImage->setDirectory($path);
        $uploadImage->setFilename('test-image.jpg');
        $fileInfo = new \SplFileInfo(self::TEST_IMAGE_FILE);

        $manager = $this->createManager();

        $resp = $manager->buildUploadedFileResponse($uploadImage, $fileInfo, $path);

        $this->assertSame(200, $resp['code']);
        $this->assertSame('', $resp['path']);
        $this->assertSame('test-image.jpg', $resp['filename']);
        $this->assertSame('test-image', $resp['relative_media_path']);

        unlink(self::TEST_IMAGE_FILE);
    }

    public function test_build_upload_response_path_4()
    {
        touch(self::TEST_IMAGE_FILE);

        $path = '.';
        $uploadImage = new MediaFile();
        $uploadImage->setDirectory($path);
        $uploadImage->setFilename('test-image.jpg');
        $fileInfo = new \SplFileInfo(self::TEST_IMAGE_FILE);

        $manager = $this->createManager();

        $resp = $manager->buildUploadedFileResponse($uploadImage, $fileInfo, $path);

        $this->assertSame(200, $resp['code']);
        $this->assertSame('', $resp['path']);
        $this->assertSame('test-image.jpg', $resp['filename']);
        $this->assertSame('test-image', $resp['relative_media_path']);

        unlink(self::TEST_IMAGE_FILE);
    }

    public function test_build_upload_response_path_5()
    {
        touch(self::TEST_IMAGE_FILE);

        $path = '/alfa/beta/gama';
        $uploadImage = new MediaFile();
        $uploadImage->setDirectory($path);
        $uploadImage->setFilename('test-image.jpg');
        $fileInfo = new \SplFileInfo(self::TEST_IMAGE_FILE);

        $manager = $this->createManager();

        $resp = $manager->buildUploadedFileResponse($uploadImage, $fileInfo, $path);

        $this->assertSame(200, $resp['code']);
        $this->assertSame('/alfa/beta/gama', $resp['path']);
        $this->assertSame('test-image.jpg', $resp['filename']);
        $this->assertSame('alfa/beta/gama/test-image', $resp['relative_media_path']);

        unlink(self::TEST_IMAGE_FILE);
    }

    /**
     * @dataProvider relativeMediaPathsProvider()
     */
    public function test_calculateRelativeMediaPath($output, $path)
    {
        touch(self::TEST_IMAGE_FILE);
        $fileInfo = new \SplFileInfo(self::TEST_IMAGE_FILE);
        $manager = $this->createManager();

        $this->assertSame($output, $manager->calculateRelativeMediaPath($fileInfo, $path), sprintf('output: %s, path: %s', $output, $path));

        unlink(self::TEST_IMAGE_FILE);
    }

    /**
     * @return \string[][]
     */
    public function relativeMediaPathsProvider()
    {
        return [
            ['test-image', ''],
            ['test-image', '.'],
            ['alfa/test-image', '/alfa'],
            ['alfa/test-image', 'alfa'],
            ['alfa/beta/test-image', '/alfa/beta'],
        ];
    }
    
    /**
     * @param string $source
     * @param string $output
     *
     * @dataProvider directoryPathsProvider
     */
    public function test_secureDirectoryPath($source, $output)
    {
        $manager = $this->createManager();

        $this->assertSame($manager->secureDirectoryPath($source), $output);
    }

    /**
     * @return array
     */
    public function directoryPathsProvider()
    {
        return [
            ['./aaaa', 'aaaa'],
            ['../bbbb', 'bbbb'],
            ['../../../..//cccc', '/cccc'],
            ['../../../../dddd', 'dddd'],
            ['/eeee', '/eeee'],
            ['/ffff/', '/ffff'],
        ];
    }

    /**
     * @group testing
     */
    public function test_getMediaFiles()
    {
        $workingDirectory = self::MEDIA_FILES_ROOT_PATH . 'testing-media/';

        $manager = $this->createManager();
        $transfer = new CreateDirectoryTransfer('/testing-media');

        $transfer->setName('two');
        $manager->createDirectory($transfer);

        $transfer->setName('one');
        $manager->createDirectory($transfer);

        touch($workingDirectory . '/beta.log');
        touch($workingDirectory . '/alfa.log');
        touch($workingDirectory . '/ai-cache');

        $user = $this->createSuperAdminMockUser();

        $items = $manager->getMediaFiles($workingDirectory, $user);

        $contentDirs = [];
        /** @var \SplFileInfo $item */
        foreach ($items['directories'] as $item) {
            $contentDirs[] = $item->getFilename();
        }

        $this->assertSame(['one', 'two'], $contentDirs);

        $contentFiles = [];
        /** @var \SplFileInfo $item */
        foreach ($items['files'] as $item) {
            $contentFiles[] = $item->getFilename();
        }

        $this->assertSame(['alfa.log', 'beta.log'], $contentFiles);
    }

    public function test_getMediaFilesProvideFile()
    {
        $manager = $this->createManager();
        $transfer = new CreateDirectoryTransfer('/');

        $transfer->setName('two');
        $manager->createDirectory($transfer);

        $transfer->setName('one');
        $manager->createDirectory($transfer);

        touch(self::MEDIA_FILES_ROOT_PATH . '/beta.log');
        touch(self::MEDIA_FILES_ROOT_PATH . '/alfa.log');

        $user = $this->createSuperAdminMockUser();
        $items = $manager->getMediaFiles(self::MEDIA_FILES_ROOT_PATH . '/beta.log', $user);

        $contentDirs = [];
        /** @var \SplFileInfo $item */
        foreach ($items['directories'] as $item) {
            $contentDirs[] = $item->getFilename();
        }

        $this->assertSame([], $contentDirs);

        $contentFiles = [];
        /** @var \SplFileInfo $item */
        foreach ($items['files'] as $item) {
            $contentFiles[] = $item->getFilename();
        }

        $this->assertSame([], $contentFiles);
    }

    public function test_buildBreadcrumb()
    {
        $manager = $this->createManager();

        $brd = $manager->buildBreadcrumb('./hello/world');

        $this->assertSame([
            [
                'name' => 'hello',
                'path' => 'hello'
            ],
            [
                'name' => 'world',
                'path' => 'hello/world'
            ],
        ], $brd);
    }

    public function test_buildBreadcrumb_empty_path()
    {
        $manager = $this->createManager();

        $brd = $manager->buildBreadcrumb('');

        $this->assertSame([], $brd);
    }

    public function test_buildBreadcrumb_previous_directories()
    {
        $manager = $this->createManager();

        $brd = $manager->buildBreadcrumb('../../../../etc/passwd');

        $this->assertSame([
            [
                'name' => 'etc',
                'path' => 'etc'
            ],
            [
                'name' => 'passwd',
                'path' => 'etc/passwd'
            ],
        ], $brd);
    }

    public function test_upload_images()
    {
        $manager = $this->createManager();

        touch(self::TEST_IMAGE_FILE);

        $uploadedFile = new UploadedFile(self::TEST_IMAGE_FILE, basename(self::TEST_IMAGE_FILE), 'image/jpeg', null, null, true);

        $request = Request::create('/', 'POST');
        $request->files->set('file', $uploadedFile);

        $mediaFile = $manager->uploadImages($request, 'alfa');

        $this->assertInstanceOf(MediaFile::class, $mediaFile);
    }

    public function test_upload_images_duplicated()
    {
        $manager = $this->createManager();

        $duplicatedFilePath = '/tmp/blank.jpg';

        copy(codecept_data_dir() . '/blank.jpg', $duplicatedFilePath);

        $uploadedFile = new UploadedFile($duplicatedFilePath, basename($duplicatedFilePath), 'image/jpeg', null, null, true);

        $request = Request::create('/', 'POST');
        $request->files->set('file', $uploadedFile);

        $manager->uploadImages($request, 'alfa');

        $this->expectException(DuplicateUploadedFile::class);
        $manager->uploadImages($request, 'alfa');
    }

    /**
     * @throws \MediaFilesBundle\Exceptions\DuplicateUploadedFile
     */
    public function test_download_image()
    {
//        if (!is_dir(self::MEDIA_FILES_ROOT_PATH . '/alfa/')) {
//            mkdir(self::MEDIA_FILES_ROOT_PATH . '/alfa/', 0775, true);
//        }

        $manager = $this->createManager();

        $url = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';

        $fileInfo = $manager->downloadImage($url, 'alfa');

        $this->assertSame('googlelogo-color-272x92dp.png', $fileInfo->getFilename());

        unlink($fileInfo->getRealPath());
    }

    /**
     * @throws \MediaFilesBundle\Exceptions\DuplicateUploadedFile
     */
    public function test_download_image_with_parameters()
    {
//        if (!is_dir(self::MEDIA_FILES_ROOT_PATH . '/alfa/')) {
//            mkdir(self::MEDIA_FILES_ROOT_PATH . '/alfa/', 0775, true);
//        }

        $manager = $this->createManager();

        $url = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png?p1=asd&asdd=3dsaas';

        $fileInfo = $manager->downloadImage($url, 'alfa');

        $this->assertSame('googlelogo-color-272x92dp.png', $fileInfo->getFilename());

        unlink($fileInfo->getRealPath());
    }

    /**
     * @throws \MediaFilesBundle\Exceptions\DuplicateUploadedFile
     */
    public function test_download_image_duplicated_file()
    {
//        if (!is_dir(self::MEDIA_FILES_ROOT_PATH . '/alfa/')) {
//            mkdir(self::MEDIA_FILES_ROOT_PATH . '/alfa/', 0775, true);
//        }

        $manager = $this->createManager();

        $url = 'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png';

        $fileInfo = $manager->downloadImage($url, 'alfa');

        $this->expectException(DuplicateUploadedFile::class);
        $manager->downloadImage($url, 'alfa');
        unlink($fileInfo->getRealPath());
    }

    public function test_rename_file()
    {
//        if (!is_dir(self::MEDIA_FILES_ROOT_PATH . '/alfa/')) {
//            mkdir(self::MEDIA_FILES_ROOT_PATH . '/alfa/', 0775, true);
//        }

        $manager = $this->createManager();

        $filePath = self::MEDIA_FILES_ROOT_PATH . '/alfa/demo-file.txt';

        if (!is_file($filePath)) {
            touch($filePath);
        }

        $splFileInfo = new \SplFileInfo($filePath);

        $output = $manager->renameFile($splFileInfo, 'demo-file-2.txt');

        $this->assertEquals(200, $output['code']);
    }

    public function test_rename_file_as_same_name()
    {
//        if (!is_dir(self::MEDIA_FILES_ROOT_PATH . '/alfa/')) {
//            mkdir(self::MEDIA_FILES_ROOT_PATH . '/alfa/', 0775, true);
//        }

        $manager = $this->createManager();

        $filePath = self::MEDIA_FILES_ROOT_PATH . '/alfa/demo-file.txt';

        if (!is_file($filePath)) {
            touch($filePath);
        }

        $splFileInfo = new \SplFileInfo($filePath);

        unlink($filePath);

        $output = $manager->renameFile($splFileInfo, 'demo-file2.txt');

        $this->assertEquals(500, $output['code']);
    }

    public function test_hidden_directory_file()
    {
        if (!is_dir(self::MEDIA_FILES_ROOT_PATH . '/alfa/.hidden')) {
            mkdir(self::MEDIA_FILES_ROOT_PATH . '/alfa/.hidden', 0775, true);
        }

        $manager = $this->createManager();

        $filePath = self::MEDIA_FILES_ROOT_PATH . 'alfa/.hidden/myfile.jpg';

        file_put_contents($filePath, 'demo file');

        $fileInfo = new \SplFileInfo($filePath);

        $this->assertSame('alfa/.hidden/myfile', $manager->calculateRelativeMediaPath($fileInfo, '/alfa/.hidden'));

        unlink($filePath);
    }

    public function test_media_files_allow_super_admin()
    {
        $request = Request::create('http://localhost', 'GET');

        $manager = $this->createManagerWithCustomRequest($request);

        $user = $this->createSuperAdminMockUser();

        $filesCollection = $manager->getMediaFiles(self::MEDIA_FILES_ROOT_PATH . 'testing/', $user);

        $this->assertCount(1, $filesCollection['directories']);
    }

    public function test_media_files_allow_super_admin_show_hidden()
    {
        $request = Request::create('http://localhost', 'GET', [
            'h' => 1
        ]);

        $manager = $this->createManagerWithCustomRequest($request);

        $user = $this->createSuperAdminMockUser();

        $filesCollection = $manager->getMediaFiles(self::MEDIA_FILES_ROOT_PATH . 'testing/', $user);

        $this->assertCount(2, $filesCollection['directories']);
    }

    public function test_media_files_allow_admin_no_hidden()
    {
        $request = Request::create('http://localhost', 'GET');

        $manager = $this->createManagerWithCustomRequest($request);

        $user = $this->createAdminMockUser();

        $filesCollection = $manager->getMediaFiles(self::MEDIA_FILES_ROOT_PATH . 'testing/', $user);

        $this->assertCount(1, $filesCollection['directories']);
    }

    public function test_media_files_allow_admin_no_hidden_with_parameter()
    {
        $request = Request::create('http://localhost', 'GET', [
            'h' => 1
        ]);

        $manager = $this->createManagerWithCustomRequest($request);

        $user = $this->createAdminMockUser();

        $filesCollection = $manager->getMediaFiles(self::MEDIA_FILES_ROOT_PATH . 'testing/', $user);

        $this->assertCount(1, $filesCollection['directories']);
    }

    public function test_media_files_allow_user()
    {
        $manager = $this->createManager();

        $user = $this->createMockUser();

        $filesCollection = $manager->getMediaFiles(self::MEDIA_FILES_ROOT_PATH . 'testing/', $user);

        $this->assertCount(1, $filesCollection['directories']);
    }

    /**
     * @return \Mockery\LegacyMockInterface|\Mockery\MockInterface|UserInterface
     */
    protected function createSuperAdminMockUser()
    {
        $user = \Mockery::mock(UserInterface::class);
        $user->shouldReceive('getRoles')->andReturns([
            'ROLE_SUPER_ADMIN'
        ]);

        return $user;
    }

    /**
     * @return \Mockery\LegacyMockInterface|\Mockery\MockInterface|UserInterface
     */
    protected function createAdminMockUser()
    {
        $user = \Mockery::mock(UserInterface::class);
        $user->shouldReceive('getRoles')->andReturns([
            'ROLE_ADMIN'
        ]);

        return $user;
    }

    /**
     * @return \Mockery\LegacyMockInterface|\Mockery\MockInterface|UserInterface
     */
    protected function createMockUser()
    {
        $user = \Mockery::mock(UserInterface::class);
        $user->shouldReceive('getRoles')->andReturns([
            'ROLE_USER'
        ]);

        return $user;
    }

    /**
     * @return \MediaFilesBundle\Services\MediaFilesManager
     */
    protected function createManager(): \MediaFilesBundle\Services\MediaFilesManager
    {
        $request = Request::createFromGlobals();
        $request = Request::create('http://localhost','GET');

        $requestStack = \Mockery::mock(RequestStack::class);
        $requestStack->shouldReceive('getMasterRequest')->andReturns($request);

        return new MediaFilesManager(self::MEDIA_FILES_ROOT_PATH, $requestStack);
    }

    /**
     * @param Request $request
     * @return MediaFilesManager
     */
    protected function createManagerWithCustomRequest(Request $request)
    {
        $requestStack = \Mockery::mock(RequestStack::class);
        $requestStack->shouldReceive('getMasterRequest')->andReturns($request);

        return new MediaFilesManager(self::MEDIA_FILES_ROOT_PATH, $requestStack);
    }
}
