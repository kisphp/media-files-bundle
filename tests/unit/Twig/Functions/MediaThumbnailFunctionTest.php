<?php

namespace Unit\MediaFilesBundle\Twig\Functions;

use Codeception\Test\Unit;
use MediaFilesBundle\Twig\Functions\MediaThumbnailFunction;

class MediaThumbnailFunctionTest extends Unit
{
    public function test_file_in_subdirectory()
    {
        $ext = (new MediaThumbnailFunction('/tmp/_media_files'))->getExtension();

        $cal = $ext->getCallable();

        $filePath = '/tmp/_media_files/alfa/one/file-1.jpg';

        $file = new \SplFileInfo($filePath);

        $url = $cal($file);

        $this->assertSame('/media-thumbs/alfa/one/file-1.jpg', $url);
    }

    public function test_file_in_root_directory()
    {
        $ext = (new MediaThumbnailFunction('/tmp/_media_files'))->getExtension();

        $cal = $ext->getCallable();

        $filePath = '/tmp/_media_files/file-3.jpg';

        $file = new \SplFileInfo($filePath);

        $url = $cal($file);

        $this->assertSame('/media-thumbs/file-3.jpg', $url);
    }
}
